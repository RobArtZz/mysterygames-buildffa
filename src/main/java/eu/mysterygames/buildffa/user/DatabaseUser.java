package eu.mysterygames.buildffa.user;

import eu.mysterygames.buildffa.BuildFFA;
import eu.mysterygames.buildffa.objects.KitType;
import eu.mysterygames.buildffa.objects.Messages;
import eu.mysterygames.gameapi.GameAPI;
import eu.mysterygames.gameapi.objects.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

/**
 * @author RobArtZz
 * 28.04.2020 21:47
 */
@Getter
@Setter
public class DatabaseUser extends MessageReceiver {
    private int kills, deaths, points, assists;
    private String key;

    private ItemStack[] rodKid = KitType.ROD.getContent().clone(),
            stickKit = KitType.STICK.getContent().clone(),
            bowKit = KitType.BOW.getContent().clone(),
            snowballKit = KitType.SNOWBALL.getContent().clone(),
            fireAspectKit = KitType.FIREASPECT.getContent().clone(),
            goldenAppleKit = KitType.GOLDENAPPLE.getContent().clone();

    public DatabaseUser(Player player) {
        super(player);
        setKey(getPlayer().getUniqueId().toString());
    }

    public void loadUser() {
        Bukkit.getScheduler().runTaskAsynchronously(BuildFFA.getInstance(), () -> {
            try {
                DatabaseUser.getUser(this);
            }catch(NullPointerException e) {
                DatabaseUser.addOrUpdateUser(this);
            }
        });
    }

    public void updateUser() {
        Bukkit.getScheduler().runTaskAsynchronously(BuildFFA.getInstance(), () -> DatabaseUser.addOrUpdateUser(this));

    }
    private static String prefix = "buildffa_";

    private static void loadKits(DatabaseUser user, String value) {
        String[] kits = value.split("/");
        for(int i = 0; i<kits.length; i++) {
            KitType type = getKitTypeByInteger(i);
            assert type != null;

            String string = kits[i];
            String[] splitted = string.split(",");

            ItemStack[] items = new ItemStack[35];
            for (String s : splitted) {
                int slot = Integer.parseInt(s.split(":")[0]);
                int id = Integer.parseInt(s.split(":")[1]);
                int amount = Integer.parseInt(s.split(":")[2]);
                ItemBuilder builder = new ItemBuilder(Material.getMaterial(id)).setAmount(amount);

                if (builder.getMaterial().equals(Material.BOW))
                    builder.setUnbreakable(true).addEnchant(Enchantment.ARROW_DAMAGE, 1);
                else if (builder.getMaterial().equals(Material.STICK))
                    builder.addEnchant(Enchantment.KNOCKBACK, 2);
                else if (builder.getMaterial().equals(Material.FISHING_ROD))
                    builder.setUnbreakable(true);
                else if (builder.getMaterial().equals(Material.GOLD_SWORD)) {
                    if (type.equals(KitType.FIREASPECT))
                        items[0] = builder.setUnbreakable(true).addEnchant(Enchantment.DAMAGE_ALL, 1).addEnchant(Enchantment.FIRE_ASPECT, 1).build();
                    else
                        items[0] = builder.setUnbreakable(true).addEnchant(Enchantment.DAMAGE_ALL, 1).build();
                }

                items[slot] = builder.build();
            }
            setKitFromType(user, type, items);
        }
    }

    private static KitType getKitTypeByInteger(int kit) {
        switch (kit) {
            case 0: return KitType.ROD;
            case 1: return KitType.STICK;
            case 2: return KitType.BOW;
            case 3: return KitType.SNOWBALL;
            case 4: return KitType.FIREASPECT;
            case 5: return KitType.GOLDENAPPLE;
            default: return null;
        }
    }

    public static void setKitFromType(DatabaseUser user, KitType type, ItemStack[] kit) {
        if(type.equals(KitType.ROD))
            user.setRodKid(kit);
        else if(type.equals(KitType.STICK))
            user.setStickKit(kit);
        else if(type.equals(KitType.BOW))
            user.setBowKit(kit);
        else if(type.equals(KitType.SNOWBALL))
            user.setSnowballKit(kit);
        else if(type.equals(KitType.FIREASPECT))
            user.setFireAspectKit(kit);
        else if(type.equals(KitType.GOLDENAPPLE))
            user.setGoldenAppleKit(kit);
    }

    public static void addOrUpdateUser(DatabaseUser user) {
        Bukkit.getConsoleSender().sendMessage(Messages.convertMessage(Messages.JEDIS_USER_UPDATE, user.getPlayer().getName()));

        StringBuilder kits = new StringBuilder();
        kits.append(convertKitToString(user.getRodKid()))
                .append("/").append(convertKitToString(user.getStickKit()))
                .append("/").append(convertKitToString(user.getBowKit()))
                .append("/").append(convertKitToString(user.getSnowballKit()))
                .append("/").append(convertKitToString(user.getFireAspectKit()))
                .append("/").append(convertKitToString(user.getGoldenAppleKit()));

        try (Jedis jedis = GameAPI.getInstance().getPool().getResource()) {
            Pipeline pipe = jedis.pipelined();
            pipe.zadd(prefix + "kills", user.getKills(), user.getKey());
            pipe.zadd(prefix + "deaths", user.getDeaths(), user.getKey());
            pipe.zadd(prefix + "assists", user.getAssists(), user.getKey());
            pipe.zadd(prefix + "points", user.getPoints(), user.getKey());
            pipe.set(prefix + user.getKey() + "_kits", kits.toString());
            pipe.sync();
        }
    }
    private static void getUser(DatabaseUser user) {
        Bukkit.getConsoleSender().sendMessage(Messages.convertMessage(Messages.JEDIS_USER_LOAD, user.getPlayer().getName()));
        Response<String> kitResponse;
        try (Jedis jedis = GameAPI.getInstance().getPool().getResource()){
            Pipeline pipe = jedis.pipelined();
            Response<Double> killResponse = pipe.zscore(prefix + "kills", user.getKey());
            Response<Double> deathResponse = pipe.zscore(prefix + "deaths", user.getKey());
            Response<Double> assistResponse = pipe.zscore(prefix + "assists", user.getKey());
            Response<Double> pointsResponse = pipe.zscore(prefix + "points", user.getKey());
            kitResponse = pipe.get(prefix + user.getKey() + "_kits");
            pipe.sync();

            user.setKills(killResponse.get().intValue());
            user.setDeaths(deathResponse.get().intValue());
            user.setPoints(pointsResponse.get().intValue());
            user.setAssists(assistResponse.get().intValue());
        }
        if(kitResponse == null)
            return;
        loadKits(user, kitResponse.get());
    }
    private static long getRank(DatabaseUser user, String key) {
        long rank = -1;
        try (Jedis jedis = GameAPI.getInstance().getPool().getResource()) {
            rank = jedis.zrevrank(prefix + key, user.getKey()) + 1;
        }
        return rank;
    }

    private static String convertKitToString(ItemStack[] items) {
        StringBuilder stringBuilder = new StringBuilder();

        boolean first = true;

        for(int i = 0; i<items.length; i++) {
            ItemStack stack = items[i];
            if(stack != null) {

                if(!first) {
                    stringBuilder.append(",");
                }

                stringBuilder.append(i).append(":").append(stack.getType().getId()).append(":").append(stack.getAmount());
                first = false;
            }
        }

        //   System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }
}