package eu.mysterygames.buildffa.user;

import eu.mysterygames.buildffa.objects.Messages;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

/**
 * @author RobArtZz
 * 29.04.2020 00:12
 */
@Getter
@Setter
@AllArgsConstructor
public class MessageReceiver {
    private Player player;

    public void sendMessage(Messages messages, String... args) {
        getPlayer().sendMessage(Messages.convertMessage(messages, args));
    }
}
