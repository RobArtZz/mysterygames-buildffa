package eu.mysterygames.buildffa.user;

import eu.mysterygames.buildffa.objects.Inventories;
import eu.mysterygames.buildffa.objects.KitType;
import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.objects.Messages;
import eu.mysterygames.gameapi.objects.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author RobArtZz
 * 28.04.2020 21:48
 */

@Getter
@Setter
public class User extends DatabaseUser {
    public static HashMap<UUID, User> users = new HashMap<>();
    private boolean build = false, inFightArea = false;

    private HashMap<User, Long> assistMap = new HashMap<>();

    private int killStreak = 0;

    public User(Player player) {
        super(player);
    }

    public void connect() {
        if(Locations.SPAWN.getLocation() != null)
            getPlayer().teleport(Locations.SPAWN.getLocation());
        setSpawnInventory();
        getPlayer().setGameMode(GameMode.SURVIVAL);
        getPlayer().setHealth(20);
        removePotionEffects();
        loadUser();
    }

    public void setBuild(boolean build) {
        this.build = build;
        if(build) {
            getPlayer().setGameMode(GameMode.CREATIVE);
            sendMessage(Messages.COMMAND_BUILD_ENABLED);
        }else {
            getPlayer().setGameMode(GameMode.SURVIVAL);
            if(Locations.SPAWN.getLocation() != null)
                getPlayer().teleport(Locations.SPAWN.getLocation());
            setSpawnInventory();
            getPlayer().setHealth(20);
            removePotionEffects();
            sendMessage(Messages.COMMAND_BUILD_DISABLED);
        }
    }

    public void disconnect() {
        users.remove(getPlayer().getUniqueId());
        updateUser();
    }

    public static User getUser(Player player) {
        if(!users.containsKey(player.getUniqueId()))
            users.put(player.getUniqueId(), new User(player));
        return users.get(player.getUniqueId());
    }

    private void removePotionEffects() {
        getPlayer().getActivePotionEffects().forEach(all -> getPlayer().removePotionEffect(all.getType()));
    }

    public void clearInventory() {
        getPlayer().getInventory().clear();
        getPlayer().getInventory().setArmorContents(null);
    }

    public void setSpawnInventory() {
        clearInventory();
        getPlayer().getInventory().setContents(Inventories.getInstance().getSpawnContent());
    }

    public void setFightInventory() {
        if(!isInFightArea()) {
            clearInventory();

            getPlayer().getInventory().setContents(getCurrentKit());
            getPlayer().getInventory().setHelmet(new ItemBuilder(Material.LEATHER_HELMET).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
            getPlayer().getInventory().setChestplate(new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).addEnchant(Enchantment.PROTECTION_PROJECTILE, 1).build());
            getPlayer().getInventory().setLeggings(new ItemBuilder(Material.LEATHER_LEGGINGS).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
            getPlayer().getInventory().setBoots(new ItemBuilder(Material.LEATHER_BOOTS).setUnbreakable(true).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());

            getPlayer().updateInventory();
        }
    }

    public ItemStack[] getCurrentKit() {
        if(KitType.getCurrent().equals(KitType.ROD))
            return getRodKid();
        else if(KitType.getCurrent().equals(KitType.BOW))
            return getBowKit();
        else if(KitType.getCurrent().equals(KitType.STICK))
            return getStickKit();
        else if(KitType.getCurrent().equals(KitType.SNOWBALL))
            return getSnowballKit();
        else if(KitType.getCurrent().equals(KitType.FIREASPECT))
            return getFireAspectKit();
        else if(KitType.getCurrent().equals(KitType.GOLDENAPPLE))
            return getGoldenAppleKit();
        return null;
    }

}