package eu.mysterygames.buildffa.user;

/**
 * @author RobArtZz
 * 29.04.2020 14:04
 */
public class Ranking {

    public static String getRankPrefix(int points) {
        // unrank
        if(points < 25)
            return "";
        // bronze
        else if(points < 50)
            return "§a❖❖❖";
        else if(points < 125)
            return "§a❖❖";
        else if(points < 225)
            return "§a❖";
        // silver
        else if(points < 400)
            return "§9❖❖❖";
        else if(points < 600)
            return "§9❖❖";
        else if(points < 1000)
            return "§9❖";
        // gold
        else if(points < 1500)
            return "§6❖❖❖";
        else if(points < 2100)
            return "§6❖❖";
        else if(points < 2700)
            return "§6❖";
        // platin
        else if(points < 3400)
            return "§c❖❖❖";
        else if(points < 4200)
            return "§c❖❖";
        else if(points < 5000)
            return "§c❖";
        // diamond
        else
            return "§b❖";
    }
}
