package eu.mysterygames.buildffa.commands;

import eu.mysterygames.buildffa.objects.Messages;
import eu.mysterygames.buildffa.objects.Permissions;
import eu.mysterygames.buildffa.user.User;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author RobArtZz
 * 28.04.2020 23:56
 */
public class CommandBuild implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Messages.convertMessage(Messages.NO_PLAYER));
            return true;
        }
        Player player = (Player) sender;
        User user = User.getUser(player);
        if(!sender.hasPermission(Permissions.COMMAND_BUILD.getPermission())) {
            user.sendMessage(Messages.NO_PERMISSION);
            return true;
        }

        user.setBuild(!user.isBuild());

        return true;
    }
}