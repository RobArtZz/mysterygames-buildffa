package eu.mysterygames.buildffa.commands;

import eu.mysterygames.buildffa.BuildFFA;
import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.objects.Messages;
import eu.mysterygames.buildffa.objects.Permissions;
import eu.mysterygames.buildffa.user.User;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author RobArtZz
 * 29.04.2020 00:00
 */
public class CommandBuildFFA implements CommandExecutor {

    private final StringBuilder usage;
    public CommandBuildFFA() {
        usage = new StringBuilder();
        usage.append("/buildffa setup <");
        for (int i = 0; i < Locations.values().length; i++) {
            if(i != 0)
                usage.append("/");
            usage.append(Locations.values()[i].name());
        }
        usage.append(">");
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Messages.convertMessage(Messages.NO_PLAYER));
            return true;
        }
        Player player = (Player) sender;
        User user = User.getUser(player);
        if(!player.hasPermission(Permissions.COMMAND_SETUP.getPermission())) {
            user.sendMessage(Messages.NO_PERMISSION);
            return true;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("setup")) {
                setLocation(args[1], user);
                return true;
            }
        }
        sendUsage(user);
        return true;
    }

    private void setLocation(String arg, User user) {
        try {
            Locations loc = Locations.valueOf(arg.toUpperCase());
            loc.setLocation(user.getPlayer().getLocation());
            Locations.saveLocation(loc, BuildFFA.getInstance().getConfig());
            user.sendMessage(Messages.COMMAND_BUILDFFA_SETUP_LOCATION, loc.name());
        }catch(Exception e) {
            sendUsage(user);
        }
    }
    private void sendUsage(User user) {
        user.sendMessage(Messages.COMMAND_SYNTAX, usage.toString());
    }
}
