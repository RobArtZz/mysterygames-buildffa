package eu.mysterygames.buildffa.objects;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Permissions {
    COMMAND_SETUP("command.buildffa.setup"),
    COMMAND_BUILD("command.buildffa.build")

    ;

    String permission;
}
