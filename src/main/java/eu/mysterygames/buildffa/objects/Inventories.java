package eu.mysterygames.buildffa.objects;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author RobArtZz
 * 28.04.2020 22:29
 */

@Getter
public class Inventories {
    private static Inventories instance;

    private ItemStack[] spawnContent;

    public Inventories(){
        instance = this;
        initialize();
    }

    private void initialize() {
        setSpawnContent();
    }

    private void setSpawnContent() {
        Inventory inv = Bukkit.createInventory(null, 3*9);

        inv.setItem(2, Items.ITEM_SORT.getItem());
        inv.setItem(4, Items.KIT_VOTING.getItem());
        inv.setItem(6, Items.STATS.getItem());

        spawnContent = inv.getContents();
    }

    public static Inventories getInstance() {
        return instance;
    }
}
