package eu.mysterygames.buildffa.objects;

import eu.mysterygames.gameapi.objects.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * @author RobArtZz
 * 28.04.2020 22:44
 */

public enum Items {

    KIT_VOTING(new ItemBuilder(Material.PAPER).setDisplayName("§eKit Voting").build()),
    STATS(new ItemBuilder(Material.CHEST).setDisplayName("§bStats").build()),
    ITEM_SORT(new ItemBuilder(Material.ENDER_CHEST).setDisplayName("§bItem Sortierung").build())
    ;

    private final ItemStack itemStack;

    Items(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public ItemStack getItem() {
        return itemStack.clone();
    }
}
