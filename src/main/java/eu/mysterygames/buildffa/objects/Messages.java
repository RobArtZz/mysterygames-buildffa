package eu.mysterygames.buildffa.objects;

import eu.mysterygames.gameapi.GameAPI;
import lombok.Getter;
import org.bukkit.ChatColor;

/**
 * @author RobArtZz
 * 28.04.2020 21:46
 */
public enum Messages {
    PREFIX(GameAPI.getPrefix(ChatColor.BLUE.toString(), "BuildFFA")),

    NO_PERMISSION(GameAPI.getNoPermissionMessage()),
    NO_PLAYER(GameAPI.getNoPlayerMessage()),

    COMMAND_SYNTAX(GameAPI.getCommandSyntax()),
    COMMAND_BUILDFFA_SETUP_LOCATION("§7Location set:§e %1$s"),
    COMMAND_BUILD_ENABLED("§7Du kannst nun bauen!"),
    COMMAND_BUILD_DISABLED("§7Du kannst nun nicht mehr bauen!"),

    PLAYER_KILLSTREAK_CANCEL("§7Der Killstreak von§9 %1$s §7wurde unterbrochen."),
    PLAYER_KILL_PLAYER("§7Du hast§9 %1$s §7getötet!"),
    PLAYER_DEATH("§7Du bist gestorben."),
    PLAYER_GOT_KILLED("§7Du wurdest von§9 %1$s §7getötet!"),
    PLAYER_GOT_ASSIST("§7Du hast dabei geholfen§9 %1$s §7zu töten."),
    PLAYER_GOT_KILLSTREAK("§9%1$s §7hat einen §9%2$ser§7-Killstreak erzielt!"),

    JEDIS_USER_LOAD("Loading database information of %1$s."),
    JEDIS_USER_UPDATE("Updating database information of %1$s."),
    ;

    @Getter
    private final String message;

    Messages(String message) {
        this.message = message;
    }

    public static String convertMessage(Messages messages, String... vars) {
        return Messages.PREFIX.getMessage() + String.format( messages.getMessage(), vars);
    }
}
