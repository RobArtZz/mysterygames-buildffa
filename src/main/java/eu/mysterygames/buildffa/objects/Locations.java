package eu.mysterygames.buildffa.objects;

import eu.mysterygames.buildffa.BuildFFA;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.IOException;

@Getter
public enum Locations {
    SPAWN,
    SAVE,
    DEATH,
    ;

    @Setter
    Location location;

    private static final String path = "locations.";

    public static void loadLocations(FileConfiguration cfg) {
        for(Locations locs : Locations.values()) {
            if(!cfg.contains(path + locs.toString())) {
                cfg.set(path + locs.toString(), null);
                continue;
            }
            locs.setLocation((Location) cfg.get(path + locs.toString()));
        }
    }
    public static void saveLocation(Locations obj, FileConfiguration cfg) {
        try {
            cfg.set(path + obj.toString(), obj.getLocation());
            cfg.save("plugins//" + BuildFFA.getInstance().getName() + "//config.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
