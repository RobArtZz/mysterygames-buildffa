package eu.mysterygames.buildffa.objects;

import eu.mysterygames.gameapi.objects.ItemBuilder;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

/**
 * @author RobArtZz
 * 29.04.2020 15:29
 */
@Getter
public enum KitType {
    ROD,
    STICK,
    BOW,
    SNOWBALL,
    FIREASPECT,
    GOLDENAPPLE;

    private ItemStack[] content;

    public static KitType current;
    KitType() {
        content = getInventoryFromType(this);
    }

    public static void setCurrent(KitType current) {
        KitType.current = current;
    }

    public static KitType getCurrent() {
        return current;
    }

    private static ItemStack[] getInventoryFromType(KitType type) {
        ItemStack[] items = new ItemStack[35];

        if(type.equals(KitType.FIREASPECT))
            items[0] = new ItemBuilder(Material.GOLD_SWORD).setUnbreakable(true).addEnchant(Enchantment.DAMAGE_ALL, 1).addEnchant(Enchantment.FIRE_ASPECT, 1).build();
        else
            items[0] = new ItemBuilder(Material.GOLD_SWORD).setUnbreakable(true).addEnchant(Enchantment.DAMAGE_ALL, 1).build();

        items[5] = new ItemBuilder(Material.SANDSTONE).setAmount(64).build();
        items[6] =  new ItemBuilder(Material.SANDSTONE).setAmount(64).build();

        items[8] =  new ItemBuilder(Material.ENDER_PEARL).build();

        if (type.equals(KitType.ROD))
            items[1] = new ItemBuilder(Material.FISHING_ROD).setUnbreakable(true).build();
        else if(type.equals(KitType.STICK))
            items[1] = new ItemBuilder(Material.STICK).addEnchant(Enchantment.KNOCKBACK, 2).build();
        else if(type.equals(KitType.BOW))
            items[1] = new ItemBuilder(Material.BOW).setUnbreakable(true).addEnchant(Enchantment.ARROW_DAMAGE, 1).build();
        else if(type.equals(KitType.GOLDENAPPLE))
            items[1] = new ItemBuilder(Material.GOLDEN_APPLE).setAmount(2).build();
        return items;
    }
}
