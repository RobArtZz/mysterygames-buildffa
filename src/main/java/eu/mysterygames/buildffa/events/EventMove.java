package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author RobArtZz
 * 29.04.2020 01:00
 */
public class EventMove implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if(Locations.DEATH.getLocation() == null || Locations.SAVE.getLocation() == null)
            return;

        if(!player.isDead()) {
            User user = User.getUser(player);

            if(player.getLocation().getY() < Locations.DEATH.getLocation().getY()) {
                if(!user.isBuild()) {
                    player.setHealth(0D);
                }
            }else if(!user.isInFightArea() && player.getLocation().getY() < Locations.SAVE.getLocation().getY()) {
                user.setFightInventory();
                user.setInFightArea(true);
            }
        }
    }
}
