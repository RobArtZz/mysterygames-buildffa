package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.user.User;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author RobArtZz
 * 29.04.2020 14:12
 */
public class EventDamage implements Listener {

    @EventHandler
    public void onDamageByEntity(EntityDamageByEntityEvent event) {
        if(!(event.getEntity() instanceof Player))
            return;
        Player player = (Player) event.getEntity();

        if(player.getLocation().getY() > Locations.SAVE.getLocation().getY() || event.getDamager().getLocation().getY() > Locations.SAVE.getLocation().getY()) {
            event.setCancelled(true);
            return;
        }
        User user = User.getUser(player);
        if(event.getDamager() instanceof Player) {
            User damager = User.getUser((Player) event.getDamager());
            addAssist(user, damager);
        }else if(event.getDamager() instanceof Projectile) {
            if (!(((Projectile) event.getDamager()).getShooter() instanceof Player))
                return;
            User damager = User.getUser(((Player) ((Projectile) event.getDamager()).getShooter()));
            addAssist(user, damager);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if(!(event.getEntity() instanceof Player))
            return;
        Player player = (Player) event.getEntity();
        if(player.getLocation().getY() > Locations.SAVE.getLocation().getY()) {
            event.setCancelled(true);
            return;
        }

        if(event.getCause().equals(EntityDamageEvent.DamageCause.FALL))
            event.setCancelled(true);
    }

    private void addAssist(User user, User damager) {
        if(user.getAssistMap().containsKey(damager)) {
            user.getAssistMap().replace(damager, System.currentTimeMillis());
        }else {
            user.getAssistMap().put(damager, System.currentTimeMillis());
        }
    }
}
