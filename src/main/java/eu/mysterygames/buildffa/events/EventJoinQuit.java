package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author RobArtZz
 * 28.04.2020 21:52
 */
public class EventJoinQuit implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        Player player = event.getPlayer();
        User user = User.getUser(player);
        user.connect();
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Player player = event.getPlayer();
        User user = User.getUser(player);
        user.disconnect();
    }
}