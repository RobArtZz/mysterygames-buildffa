package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.BuildFFA;
import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.objects.Messages;
import eu.mysterygames.buildffa.user.User;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * @author RobArtZz
 * 29.04.2020 00:41
 */
public class EventDeath implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        event.setDeathMessage(null);
        event.setKeepInventory(true);
        event.setKeepLevel(true);

        Player player = event.getEntity();
        User user = User.getUser(player);
        user.setDeaths(user.getDeaths() + 1);
        user.setPoints(user.getPoints() + BuildFFA.getInstance().getDeathPoints());
        if(user.getKillStreak() >= 5) {
            Bukkit.getConsoleSender().sendMessage(Messages.convertMessage(Messages.PLAYER_KILLSTREAK_CANCEL, user.getPlayer().getName()));
        }
        user.setKillStreak(0);
        player.playSound(player.getLocation(), Sound.NOTE_BASS, 1f, 1f);

        if(player.getKiller() == null) {
            user.sendMessage(Messages.PLAYER_DEATH);
        }else {
            User killer = User.getUser(player.getKiller());
            killer.getPlayer().setHealth(20D);
            killer.setKills(killer.getKills() + 1);
            killer.setPoints(killer.getPoints() + BuildFFA.getInstance().getKillPoints());
            killer.setKillStreak(killer.getKillStreak() + 1);

            if(killer.getKillStreak() % 5 == 0) {
                Bukkit.broadcastMessage(Messages.convertMessage(Messages.PLAYER_GOT_KILLSTREAK, killer.getPlayer().getName(), String.valueOf(killer.getKillStreak())));
                killer.setPoints(killer.getPoints() + BuildFFA.getInstance().getKillstreakPoints());
            }

            user.sendMessage(Messages.PLAYER_GOT_KILLED, killer.getPlayer().getName());
            killer.sendMessage(Messages.PLAYER_KILL_PLAYER, user.getPlayer().getName());

            killer.getPlayer().playSound(killer.getPlayer().getLocation(), Sound.NOTE_PLING, 1f, 1f);

            handleAssists(user, killer);
        }
        user.getAssistMap().clear();

        Bukkit.getScheduler().runTaskLater(BuildFFA.getInstance(), () -> player.spigot().respawn(), 15L);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        User user = User.getUser(player);
        user.setInFightArea(false);
        if(Locations.SPAWN.getLocation() != null)
            event.setRespawnLocation(Locations.SPAWN.getLocation());
        user.setSpawnInventory();
    }

    private void handleAssists(User user, User killer) {
        user.getAssistMap().forEach((users, time) -> {
            if(!users.getPlayer().getUniqueId().equals(killer.getPlayer().getUniqueId())
                    && isDelayed(time, 5000L)) {
                users.sendMessage(Messages.PLAYER_GOT_ASSIST, user.getPlayer().getName());
                users.setAssists(users.getAssists() + 1);
                users.getPlayer().playSound(killer.getPlayer().getLocation(), Sound.NOTE_PLING, 1f, 1f);
            }
        });
    }

    private boolean isDelayed(long value, long time) {
        return System.currentTimeMillis() - value > time;
    }
}
