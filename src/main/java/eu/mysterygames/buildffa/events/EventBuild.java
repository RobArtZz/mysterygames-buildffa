package eu.mysterygames.buildffa.events;

import eu.mysterygames.buildffa.BuildFFA;
import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.user.User;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author RobArtZz
 * 28.04.2020 22:22
 */
public class EventBuild implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        User user = User.getUser(player);
        if(user.isBuild())
            return;

        if(player.getLocation().getY() > Locations.SAVE.getLocation().getY()) {
            event.setCancelled(true);
            return;
        }

        if(event.getItemInHand().getType().equals(Material.SANDSTONE)) {
            event.getItemInHand().setAmount(64);
            player.updateInventory();
        }

        if(!event.getBlock().getType().equals(Material.LADDER) && !event.getBlock().getType().equals(Material.WEB)){
            new BukkitRunnable() {
                @Override
                public void run() {
                    event.getBlock().setType(Material.REDSTONE_BLOCK);
                }
            }.runTaskLater(BuildFFA.getInstance(), 20L * 3);
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                event.getBlock().setType(Material.AIR);
                event.getBlock().getWorld().playEffect(event.getBlock().getLocation(), Effect.EXTINGUISH, 1);
            }
        }.runTaskLater(BuildFFA.getInstance(), 20L * 5);

    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        User user = User.getUser(player);
        if(!user.isBuild())
            event.setCancelled(true);
    }
}
