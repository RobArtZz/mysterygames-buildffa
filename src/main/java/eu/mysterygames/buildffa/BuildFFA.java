package eu.mysterygames.buildffa;

import eu.mysterygames.buildffa.commands.CommandBuild;
import eu.mysterygames.buildffa.commands.CommandBuildFFA;
import eu.mysterygames.buildffa.events.*;
import eu.mysterygames.buildffa.objects.Inventories;
import eu.mysterygames.buildffa.objects.KitType;
import eu.mysterygames.buildffa.objects.Locations;
import eu.mysterygames.buildffa.user.DatabaseUser;
import eu.mysterygames.buildffa.user.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author RobArtZz
 * 29.04.2020 00:10
 */
@Getter
@Setter
public class BuildFFA extends JavaPlugin {
    private static BuildFFA instance;

    private int killPoints = 10, deathPoints = -5, killstreakPoints = 5;

    @Override
    public void onEnable() {
        super.onEnable();
        initialize();
    }

    private void initialize() {
        instance = this;
        saveDefaultConfig();

        new Inventories();

        getServer().getPluginManager().registerEvents(new EventJoinQuit(), this);
        getServer().getPluginManager().registerEvents(new EventBuild(), this);
        getServer().getPluginManager().registerEvents(new EventOther(), this);
        getServer().getPluginManager().registerEvents(new EventDeath(), this);
        getServer().getPluginManager().registerEvents(new EventDamage(), this);
        getServer().getPluginManager().registerEvents(new EventMove(), this);

        getCommand("build").setExecutor(new CommandBuild());
        getCommand("buildffa").setExecutor(new CommandBuildFFA());

        Locations.loadLocations(getConfig());


        KitType.setCurrent(KitType.STICK);

        //TODO: UPDATE DATABASE LIKE THIS (EVERY MINUTE) OR AFTER EVERY KILL -> TALK WITH LUISA
        Bukkit.getScheduler().runTaskTimerAsynchronously(BuildFFA.getInstance(), new Runnable() {
            @Override
            public void run() {
                User.users.values().forEach(DatabaseUser::addOrUpdateUser);
            }
        }, 20L * 60, 20L * 60);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public static BuildFFA getInstance() {
        return instance;
    }
}
